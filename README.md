# NAME

BamFdw

# DESCRIPTION

This is a wrapper around pysam developed for Multicorn and postgres.

For the most complete and up to date documentation please see the following.

https://wcmc-q.atlassian.net/wiki/display/GPF/BAM+File+FDW

# SQL Code

## Create the server

```sql
    CREATE SERVER bam_srv foreign data wrapper multicorn options (
        wrapper 'bamfdw.bammeta.BamFdw'
        );
```

## Create the table

```sql
    DROP FOREIGN TABLE bam_core;
    CREATE FOREIGN TABLE bam_core (
        "contig" VARCHAR(50),
        "reference_start" INT,
        "reference_end" INT,
        "query_alignment_start" INT,
        "query_alignment_end" INT,
        "cigarstring" VARCHAR(200),
        "is_paired" boolean,
        "is_reverse" boolean,
        "query_sequence" text,
        "query_qualities" integer ARRAY,
        "mapping_quality" INT
    ) server bam_srv options(
        bamfile '/data/coredata/advcomp/1Kbam/raw_data/bam/HG00096.mapped.illumina.mosaik.GBR.exome.20110411.bam',
        replchrom 'True',
        fastafile '/data/coredata/advcomp/1Kbam/raw_data/fasta/human_g1k_v37.fasta',
        bedfile '/data/coredata/advcomp/1Kbam/raw_data/bed/20110225.called_exome_targets.consensus.100.bed'
    );
```

## Run a quick test

```sql
    select * from bam_core limit 10;
```

## Create some functions


### Supply contig, start, end

```sql
    DROP FUNCTION inputtest(text,integer,integer);
    CREATE OR REPLACE FUNCTION inputtest(ccontig text, cstart int, cend int) RETURNS SETOF bam_core as 
    $BODY$
    DECLARE
        brow bam_core%rowtype;
    BEGIN
        PERFORM FROM bam_core
            WHERE bam_core.contig = ccontig
            AND bam_core.reference_start >= cstart 
            AND bam_core.reference_end <= cend; 
        RETURN NEXT brow;
    END;
    $BODY$
    LANGUAGE 'plpgsql' ;
```

### Create a function that loops through a table of coordinates, in this case the called_exome_targets_20110225 from 1KGenomes.

```sql
    CREATE OR REPLACE FUNCTION bammeta() RETURNS SETOF bam_core AS
    $BODY$
    DECLARE
        crow called_exome_targets_20110225%rowtype;
        brow bam_core%rowtype;
    BEGIN
        FOR crow IN SELECT * FROM called_exome_targets_20110225 
        LOOP
            PERFORM * FROM bam_core
            WHERE bam_core.contig = crow.contig
            AND bam_core.reference_start >= crow.start 
            AND bam_core.reference_end <= crow.end; 
        END LOOP;
    END;
    $BODY$
    LANGUAGE 'plpgsql' ;

    SELECT * FROM bammeta() LIMIT 100;
```

# ACKNOWLEDGEMENTS

This module was originally developed at and for Weill Cornell Medical College in Qatar. With approval from WCMC-Q, this information was generalized and put on bitbucket, for which the authors would like to express their gratitude.


