import pysam

class BamViewer(object):

    def __init__(self, ifile):
        self.sam = pysam.AlignmentFile(ifile, 'rb')
# postgres table also has contig
        self.query = ['query_alignment_start',
                      'query_alignment_end',
                      'cigarstring',
                      'is_paired',
                      'is_reverse',
                      'reference_start',
                      'reference_end',
                      'query_sequence',
                      'query_qualities',
                      'mapping_quality']

    def getvals(self):
        return self.query

    def close(self):
        self.sam.close()

    def bammeta(self, contig, start, end):
        i = self.sam.pileup(
            contig, int(start), int(end), multiple_iterators=True)

        for pileupcolumn in i:
            for pileupread in pileupcolumn.pileups:
                p = pileupread.alignment
                result = {}
                result['contig'] = contig
                for v in self.query:
                    r = getattr(p, v)
                    result[v] = r
#                #print result
                yield result

    def bamcov(self, contig, start, end):
        i = self.sam.pileup(
            str(contig), int(start), int(end), multiple_iterators=True)
        while True:
            try:
                pileupcolumn = i.next()
                result = {}
                pos = pileupcolumn.pos
                cov = pileupcolumn.n
                # print 'in producer %d ' % pileupcolumn.pos
                result['contig'] = str(contig)
                result['reference_start'] = pos
                result['reference_end'] = pos
                #result['pos'] = pos
                result['cov'] = cov
                yield result
            except StopIteration:
                break


def view(ifile):
    return BamViewer(ifile)

# bammeta returns an object that looks like this:

# {'query_alignment_start': 0, 'cigarstring': '100M',
# 'mapping_quality': 66, 'reference_end': 987122,
# 'is_reverse': False, 'is_paired': True, 'query_alignment_end': 100,
# 'query_qualities': array('B', [40, 36, 36, 36, 35, 38, 36, 38, 37, 38, 39, 39,
#     39, 38, 38, 38, 37, 39, 39, 39, 29, 37, 39, 40, 40, 39, 39, 37, 31, 35,
#     36, 40, 39, 39, 36, 32, 40, 38, 36, 40, 40, 40, 39, 40, 40, 33, 33, 37,
#     40, 37, 34, 40, 39, 35, 32, 36, 37, 40, 40, 26, 36, 32, 23, 30, 32, 33,
#     33, 33, 26, 30, 34, 35, 36, 33, 31, 26, 28, 31, 25, 31, 34, 35, 31, 4, 4,
#     4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4]),
# 'query_sequence': 'AGGGTGAGCCTGGCACAGGGCAGGGGGCGGAGGCCGGATGGGCCCGGAGCC
#     CACGAGGCCCCACCCTCACCTGCCTATCTCACAGGGCTGGTGGAGAAGT',
#  'chrom': '1', 'reference_start': 987022}
