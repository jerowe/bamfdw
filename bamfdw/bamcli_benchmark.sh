echo ==========================================
echo BamMetaCLI Jobs
echo "" 

echo "100 seq 1 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j1 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "100 seq 4 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j4 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "100 seq 8 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j8 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "100 seq 16 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j16 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "1000 seq 1 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j1 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "1000 seq 4 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j4 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "1000 seq 8 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j8 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "1000 seq 16 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j16 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "10000 seq 1 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j1 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "10000 seq 4 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j4 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "10000 seq 8 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j8 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "10000 seq 16 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j16 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "All the seq 1 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j1 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "All the seq 4 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j4 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "All the seq 8 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j8 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo "All the seq 16 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j16 -q bash -c "python bammetacli.py {}" ;  )
echo "" 

echo ==========================================
echo BamCovCLI Jobs
echo "" 

echo "100 seq 1 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j1 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "100 seq 4 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j4 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "100 seq 8 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j8 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "100 seq 16 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j16 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "1000 seq 1 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j1 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "1000 seq 4 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j4 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "1000 seq 8 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j8 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "1000 seq 16 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j16 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "10000 seq 1 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j1 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "10000 seq 4 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j4 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "10000 seq 8 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j8 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "10000 seq 16 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j16 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "All the seq 1 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j1 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "All the seq 4 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j4 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "All the seq 8 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j8 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo "All the seq 16 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print $1","$2","$3}' | parallel -n 1 -j16 -q bash -c "python bamcovcli.py {}" ;  )
echo "" 

echo ==========================================
echo BamCovPostgres Jobs
echo ""

echo "All the seq 1 semaphore"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j1 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
echo ""

echo "All the seq 4 semaphore"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j4 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
echo ""

echo "All the seq 8 semaphore"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j8 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
echo ""

echo "All the seq 16 semaphore"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j16 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
echo ""


echo ==========================================
echo All done! 
echo ""
