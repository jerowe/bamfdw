from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres
from logging import WARNING
import re
import operator
from collections import defaultdict
from bamfdw import bamanalysis

class BamMetaFdw(ForeignDataWrapper):


    """A foreign data wrapper for accessing bam files
        Valid options:
            -bam/cram file
            -fasta file
            -replchrom TRUE/FALSE
            -bedfile
    """

    def with_seq(self, quals, columns):
        #log_to_postgres("with seq")

        contig = []
        start = []
        end = []

        # Going to decide that we don't actually care about conditions for seq.
        # If there is a start/end we're going to assume it is start > X < end
        # 2.0 will include qualifiers for mapping/quality/whatever conditions

        clist = self.clauses.get('contig')
        for c in clist:
            for k, v in c.items():
                contig.append(k)

        clist = self.clauses.get('reference_start')
        for c in clist:
            for k, v in c.items():
                start.append(k)

        clist = self.clauses.get('reference_end')
        for c in clist:
            for k, v in c.items():
                end.append(k)

        for c in contig:
            c = str(c)
            if self.replchorm:
                c = c.replace('chr', '')
            for s in start:
                for e in end:
                    cols = [c, int(s), int(e)]
                    yield cols

    def no_seq(self, quals, columns):

        f = file(self.bedfile,
                 'r')

        for line in f:
            t = line
            t = t.strip()
            cols = t.split()
            t = cols[0]
            t = str(t)
            if self.replchorm:
                t = t.replace('chr', '')
            cols[0] = t
            yield cols

        f.close()

    def repr_quals(self, quals, columns):
        self.clauses.clear()
        for qual in quals:
            #log_to_postgres("Qual is %s " % qual)
            operator = self.operators.get(qual.operator, None)
            if qual.field_name == 'contig':
                t = qual.value
                t = t.replace('chr', '')
                qual.value = t
            if operator:
                self.clauses[qual.field_name].append({qual.value: operator})

        if self.clauses.get('contig') and self.clauses.get('reference_start') and self.clauses.get('reference_end'):
            #log_to_postgres("We have a contig, start, end!")
            return self.with_seq(quals, columns)
        elif self.clauses.get('contig') and self.clauses.get('reference_end'):
            self.clauses['reference_start'].append({1: self.operators.get('=')})
            #log_to_postgres("There is a contig and an end!")
            return self.with_seq(quals, columns)
        elif self.clauses.get('contig') and self.clauses.get('reference_start'):
            clist = self.clauses.get('contig')
            for i in clist:
                #log_to_postgres("items are ")
                #log_to_postgres(i)
                for k,v in i.items():
                    #log_to_postgres("K is %s and V is %s " % (v, k))
                    self.clauses['reference_end'].append({self.hg19cord.get(k) : self.operators.get('=')})
            return self.with_seq(quals, columns)
        else:
            #log_to_postgres(" You did not supply a reference. "
                            #" Iterating through entire bed file. "
                            #" This will take awhile are you sure? ")
            return self.no_seq(quals, columns)

    def __init__(self, fdw_options, fdw_columns):
        super(BamMetaFdw, self).__init__(fdw_options, fdw_columns)
        self.bamfile = fdw_options["bamfile"]
        self.fastafile = fdw_options["fastafile"]
        self.replchrom = fdw_options["replchrom"]
        self.bedfile = fdw_options["bedfile"]
        self.columns = fdw_columns
        self.operators = {
            '=': operator.eq,
            '<': operator.lt,
            '>': operator.gt,
            '<=': operator.le,
            '>=': operator.ge,
            '<>': operator.ne,
            '~~': re.match,
            '~~*': re.match,
            ('=', True): operator.truth,
            ('<>', False): operator.is_not
        }
        self.clauses = defaultdict(list)
        self.hg19cord = {
            '1':    249250621,
            '2':    243199373,
            '3':    198022430,
            '4':    191154276,
            '5':    180915260,
            '6':    171115067,
            '7':    159138663,
            'X':    155270560,
            '8':    146364022,
            '9':    141213431,
            '10':   135534747,
            '11':   135006516,
            '12':   133851895,
            '13':   115169878,
            '14':   107349540,
            '15':   102531392,
            '16':   90354753,
            '17':   81195210,
            '18':   78077248,
            '20':   63025520,
            'Y':   59373566,
            '19':   59128983,
            '22':   51304566,
            '21':   48129895
        }
        self.samfile = bamanalysis.view(self.bamfile)

    def execute(self, quals, columns):
        #log_to_postgres("Quals")
        #log_to_postgres(str(sorted(quals)))
        # log_to_postgres(str(sorted(columns)))

        for cols in self.repr_quals(quals, columns):
            #log_to_postgres("Cols are ")
            #log_to_postgres(cols)
            results = self.samfile.bammeta(cols[0], cols[1], cols[2])
            for r in results:
                yield r
