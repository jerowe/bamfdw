from bamfdw import bamanalysis
import sys

cstr = sys.argv[1]
coord = cstr.split(',')

samfile = bamanalysis.view('/data/coredata/advcomp/1Kbam/raw_data/bam/HG00096.mapped.illumina.mosaik.GBR.exome.20110411.bam')
results = samfile.bammeta(coord[0], coord[1], coord[2])

for r in results:
    pass
