DROP SERVER bamcov_srv;

CREATE SERVER bamcov_srv foreign data wrapper multicorn options (
    wrapper 'bamfdw.bamcov.BamCovFdw'
    );

DROP FOREIGN TABLE bamcov cascade;
CREATE FOREIGN TABLE bamcov (
    "contig" VARCHAR(50),
    "reference_start" INT,
    "reference_end" INT,
    "cov" INT
) server bamcov_srv options(
    bamfile '/data/coredata/advcomp/1Kbam/raw_data/bam/HG00096.mapped.illumina.mosaik.GBR.exome.20110411.bam',
    replchrom 'True',
    fastafile '/data/coredata/advcomp/1Kbam/raw_data/fasta/human_g1k_v37.fasta',
    bedfile '/data/coredata/advcomp/1Kbam/raw_data/bed/20110225.called_exome_targets.consensus.bed'
);

select * from bamcov limit 10;
select * from bamcov where contig = '2' limit 10;

CREATE OR REPLACE FUNCTION bamcovfun() RETURNS SETOF bamcov AS
$BODY$
DECLARE
    crow called_exome_targets_20110225%rowtype;
    brow bamcov%rowtype;
BEGIN
    FOR crow IN SELECT * FROM called_exome_targets_20110225 
    LOOP
        PERFORM * FROM bamcov
           WHERE bamcov.contig = crow.contig
           AND bamcov.reference_start >= crow.start 
           AND bamcov.reference_end <= crow.end; 
    END LOOP;
END;
$BODY$
LANGUAGE 'plpgsql' ;

SELECT * FROM bamcovfun();


DROP FUNCTION bamcovinputfun(text,integer,integer);
CREATE OR REPLACE FUNCTION bamcovinputfun(ccontig text, cstart int, cend int) RETURNS SETOF bamcov as 
$BODY$
DECLARE
    brow bamcov%rowtype;
BEGIN
    PERFORM FROM bamcov
        WHERE bamcov.contig = ccontig
        AND bamcov.reference_start >= cstart 
        AND bamcov.reference_end <= cend; 
    RETURN NEXT brow;
END;
$BODY$
LANGUAGE 'plpgsql' ;

SELECT * FROM bamcovinputfun('1', 69503, 69514);

time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)


/*CStore things!*/

-- load extension first time after install
CREATE EXTENSION cstore_fdw;

-- create server object
CREATE SERVER cstore_server FOREIGN DATA WRAPPER cstore_fdw;

CREATE FOREIGN TABLE cstorebamcovHG00096 (
    "contig" VARCHAR(50),
    "reference_start" INT,
    "reference_end" INT,
    "cov" INT
)
SERVER cstore_server
OPTIONS(compression 'pglz');

INSERT INTO cstorebamcovHG00096 SELECT * from HG00096;

-- /*Do a benchmark for timings of cstore*/

DROP FUNCTION cstorebamcovHG00096fun();
CREATE OR REPLACE FUNCTION cstorebamcovHG00096fun() RETURNS SETOF cstorebamcovHG00096 as 
$BODY$
DECLARE
    brow cstorebamcovHG00096%rowtype;
BEGIN
    PERFORM * FROM cstorebamcovHG00096;
END;
$BODY$
LANGUAGE 'plpgsql' ;

SELECT * FROM cstorebamcovHG00096fun();

/*Time: 908.797 ms*/


