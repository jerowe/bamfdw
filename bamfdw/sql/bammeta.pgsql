/*# {'query_alignment_start': 0, 'cigarstring': '100M',*/
/*# 'mapping_quality': 66, 'reference_end': 987122,*/
/*# 'is_reverse': False, 'is_paired': True, 'query_alignment_end': 100,*/
/*# 'query_qualities': array('B', [40, 36, 36, 36, 35, 38, 36, 38, 37, 38, 39, 39,*/
/*#     39, 38, 38, 38, 37, 39, 39, 39, 29, 37, 39, 40, 40, 39, 39, 37, 31, 35,*/
/*#     36, 40, 39, 39, 36, 32, 40, 38, 36, 40, 40, 40, 39, 40, 40, 33, 33, 37,*/
/*#     40, 37, 34, 40, 39, 35, 32, 36, 37, 40, 40, 26, 36, 32, 23, 30, 32, 33,*/
/*#     33, 33, 26, 30, 34, 35, 36, 33, 31, 26, 28, 31, 25, 31, 34, 35, 31, 4, 4,*/
/*#     4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4]),*/
/*# 'query_sequence': 'AGGGTGAGCCTGGCACAGGGCAGGGGGCGGAGGCCGGATGGGCCCGGAGCC*/
/*#     CACGAGGCCCCACCCTCACCTGCCTATCTCACAGGGCTGGTGGAGAAGT',*/
/*#  'chrom': '1', 'reference_start': 987022}*/
    /*self.query = ['query_alignment_start',*/
                    /*'query_alignment_end',*/
                    /*'cigarstring',*/
                    /*'is_paired',*/
                    /*'is_reverse',*/
                    /*'reference_start',*/
                    /*'reference_end',*/
                    /*'query_sequence',*/
                    /*'query_qualities',*/
                    /*'mapping_quality']*/


CREATE SERVER bammeta_srv foreign data wrapper multicorn options (
    wrapper 'bamfdw.bammeta.BamMetaFdw'
);

DROP FOREIGN TABLE bam_core;
CREATE FOREIGN TABLE bam_core (
    "contig" VARCHAR(50),
    "reference_start" INT,
    "reference_end" INT,
    "query_alignment_start" INT,
    "query_alignment_end" INT,
    "cigarstring" VARCHAR(200),
    "is_paired" boolean,
    "is_reverse" boolean,
    "query_sequence" text,
    "query_qualities" integer ARRAY,
    "mapping_quality" INT
) server bammeta_srv options(
    bamfile '/data/coredata/advcomp/1Kbam/raw_data/bam/HG00096.mapped.illumina.mosaik.GBR.exome.20110411.bam',
    replchrom 'True',
    fastafile '/data/coredata/advcomp/1Kbam/raw_data/fasta/human_g1k_v37.fasta',
    bedfile '/data/coredata/advcomp/1Kbam/raw_data/bed/20110225.called_exome_targets.consensus.100.bed'
);

select * from bam_core limit 10;
/*select * from bam_core where contig = '1' and reference_start > 861321 and reference_end < 861393 limit 1;*/
/*SELECT * FROM bam_core where reference_end <= 70008 AND contig = '1' AND reference_start > 69090;*/

/*select * from called_exome_targets_20110225 c limit 10;*/

DROP FUNCTION inputtest(text,integer,integer);
CREATE OR REPLACE FUNCTION inputtest(ccontig text, cstart int, cend int) RETURNS SETOF bam_core as 
$BODY$
DECLARE
    brow bam_core%rowtype;
BEGIN
    PERFORM FROM bam_core
        WHERE bam_core.contig = ccontig
        AND bam_core.reference_start >= cstart 
        AND bam_core.reference_end <= cend; 
    RETURN NEXT brow;
END;
$BODY$
LANGUAGE 'plpgsql' ;

CREATE OR REPLACE FUNCTION bammeta() RETURNS SETOF bam_core AS
$BODY$
DECLARE
    crow called_exome_targets_20110225%rowtype;
    brow bam_core%rowtype;
BEGIN
    FOR crow IN SELECT * FROM called_exome_targets_20110225 
    LOOP
        PERFORM * FROM bam_core
           WHERE bam_core.contig = crow.contig
           AND bam_core.reference_start >= crow.start 
           AND bam_core.reference_end <= crow.end; 
    END LOOP;
END;
$BODY$
LANGUAGE 'plpgsql' ;

/*SELECT * FROM bammeta() LIMIT 100;*/

psql -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | xargs -0 -I {} bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \" "

time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
/*# {'query_alignment_start': 0, 'cigarstring': '100M',*/
/*# 'mapping_quality': 66, 'reference_end': 987122,*/
/*# 'is_reverse': False, 'is_paired': True, 'query_alignment_end': 100,*/
/*# 'query_qualities': array('B', [40, 36, 36, 36, 35, 38, 36, 38, 37, 38, 39, 39,*/
/*#     39, 38, 38, 38, 37, 39, 39, 39, 29, 37, 39, 40, 40, 39, 39, 37, 31, 35,*/
/*#     36, 40, 39, 39, 36, 32, 40, 38, 36, 40, 40, 40, 39, 40, 40, 33, 33, 37,*/
/*#     40, 37, 34, 40, 39, 35, 32, 36, 37, 40, 40, 26, 36, 32, 23, 30, 32, 33,*/
/*#     33, 33, 26, 30, 34, 35, 36, 33, 31, 26, 28, 31, 25, 31, 34, 35, 31, 4, 4,*/
/*#     4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4]),*/
/*# 'query_sequence': 'AGGGTGAGCCTGGCACAGGGCAGGGGGCGGAGGCCGGATGGGCCCGGAGCC*/
/*#     CACGAGGCCCCACCCTCACCTGCCTATCTCACAGGGCTGGTGGAGAAGT',*/
/*#  'chrom': '1', 'reference_start': 987022}*/
    /*self.query = ['query_alignment_start',*/
                    /*'query_alignment_end',*/
                    /*'cigarstring',*/
                    /*'is_paired',*/
                    /*'is_reverse',*/
                    /*'reference_start',*/
                    /*'reference_end',*/
                    /*'query_sequence',*/
                    /*'query_qualities',*/
                    /*'mapping_quality']*/

CREATE SERVER bammeta_srv foreign data wrapper multicorn options (
    wrapper 'bamfdw.bammeta.BamMetaFdw'
    );

DROP FOREIGN TABLE bam_core;
CREATE FOREIGN TABLE bam_core (
    "contig" VARCHAR(50),
    "reference_start" INT,
    "reference_end" INT,
    "query_alignment_start" INT,
    "query_alignment_end" INT,
    "cigarstring" VARCHAR(200),
    "is_paired" boolean,
    "is_reverse" boolean,
    "query_sequence" text,
    "query_qualities" integer ARRAY,
    "mapping_quality" INT
) server bam_srv options(
    bamfile '/data/coredata/advcomp/1Kbam/raw_data/bam/HG00096.mapped.illumina.mosaik.GBR.exome.20110411.bam',
    replchrom 'True',
    fastafile '/data/coredata/advcomp/1Kbam/raw_data/fasta/human_g1k_v37.fasta',
    bedfile '/data/coredata/advcomp/1Kbam/raw_data/bed/20110225.called_exome_targets.consensus.100.bed'
);

select * from bam_core limit 10;
/*select * from bam_core where contig = '1' and reference_start > 861321 and reference_end < 861393 limit 1;*/
/*SELECT * FROM bam_core where reference_end <= 70008 AND contig = '1' AND reference_start > 69090;*/

/*select * from called_exome_targets_20110225 c limit 10;*/

DROP FUNCTION bammetainput(text,integer,integer);
CREATE OR REPLACE FUNCTION bammetainput(ccontig text, cstart int, cend int) RETURNS SETOF bam_core as 
$BODY$
DECLARE
    brow bam_core%rowtype;
BEGIN
    PERFORM FROM bam_core
        WHERE bam_core.contig = ccontig
        AND bam_core.reference_start >= cstart 
        AND bam_core.reference_end <= cend; 
    RETURN NEXT brow;
END;
$BODY$
LANGUAGE 'plpgsql' ;

CREATE OR REPLACE FUNCTION bammeta() RETURNS SETOF bam_core AS
$BODY$
DECLARE
    crow called_exome_targets_20110225%rowtype;
    brow bam_core%rowtype;
BEGIN
    FOR crow IN SELECT * FROM called_exome_targets_20110225 
    LOOP
        PERFORM * FROM bam_core
           WHERE bam_core.contig = crow.contig
           AND bam_core.reference_start >= crow.start 
           AND bam_core.reference_end <= crow.end; 
    END LOOP;
END;
$BODY$
LANGUAGE 'plpgsql' ;

/*SELECT * FROM bammeta() LIMIT 100;*/

/*psql -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | xargs -0 -I {} bash -c "psql jir2004 -c \"SELECT * FROM bammetaintput({}) \" "*/

/*time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM bammetaintput({}) \"" > /dev/null 2>&1)*/


/*CStore things!*/

-- load extension first time after install
CREATE EXTENSION cstore_fdw;

-- create server object
CREATE SERVER cstore_server FOREIGN DATA WRAPPER cstore_fdw;

CREATE FOREIGN TABLE cstorebammetaHG00096 (
    "contig" VARCHAR(50),
    "reference_start" INT,
    "reference_end" INT,
    "query_alignment_start" INT,
    "query_alignment_end" INT,
    "cigarstring" VARCHAR(200),
    "is_paired" boolean,
    "is_reverse" boolean,
    "query_sequence" text,
    "query_qualities" integer ARRAY,
    "mapping_quality" INT
)
SERVER cstore_server
OPTIONS(compression 'pglz');

INSERT INTO cstorebammetaHG00096 SELECT * from HG00096;

-- /*Do a benchmark for timings of cstore*/

DROP FUNCTION cstorebammetaHG00096fun();
CREATE OR REPLACE FUNCTION cstorebammetaHG00096fun() RETURNS SETOF cstorebammetaHG00096 as 
$BODY$
DECLARE
    brow cstorebammetaHG00096%rowtype;
BEGIN
    PERFORM * FROM cstorebammetaHG00096;
END;
$BODY$
LANGUAGE 'plpgsql' ;

SELECT * FROM cstorebammetaHG00096fun();

/*Time: 908.797 ms*/


