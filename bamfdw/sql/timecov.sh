#!/bin/bash - 
#===============================================================================
#
#          FILE: timeit.sh
# 
#         USAGE: ./timeit.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 04/01/2015 13:46
#      REVISION:  ---
#===============================================================================

# #set -o nounset                              # Treat unset variables as an error
# 
# # 10 seq
# # ==========================================
# 
# echo "10 seq 1 semaphore"
# time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j1 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
# 
# echo "10 seq 4 semaphore"
# time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j4 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
# 
# echo "10 seq 8 semaphore"
# time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j8 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
# 
# echo "10 seq 16 semaphore"
# time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j16 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
# 
# 
# # ==========================================
# 
# 
# # 100 seq
# # ==========================================
# 
# echo "100 seq 1 semaphore"
# time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j1 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
# 
# echo "100 seq 4 semaphore"
# time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j4 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
# 
# echo "100 seq 8 semaphore"
# time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j8 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
# 
# echo "100 seq 16 semaphore"
# time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j16 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
# 
# # ==========================================
# 
# # 1000 seq
# # ==========================================
# 
# echo "1000 seq 1 semaphore"
# time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j1 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
# 
# echo "1000 seq 4 semaphore"
# time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j4 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
# 
# echo "1000 seq 8 semaphore"
# time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j8 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
# 
# echo "1000 seq 16 semaphore"
# time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j16 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)
# 
# # ==========================================

echo "10000"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j1 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)

echo "10000"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j4 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)

echo "10000"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j8 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)

echo "10000"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j16 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)

# All of them
# ==========================================

echo "All the seq 1 semaphore"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j1 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)

echo "All the seq 4 semaphore"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j4 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)

echo "All the seq 8 semaphore"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j8 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)

echo "All the seq 16 semaphore"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -n 1 -j16 -q bash -c "psql jir2004 -c \"SELECT * FROM bamcovinputfun({}) \"" > /dev/null 2>&1)

