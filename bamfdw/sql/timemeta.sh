#!/bin/bash - 

### 100 seq
echo "100 regions 1 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j1 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""

echo "100 regions 4 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j4 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""

echo "100 regions 8 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j8 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""


echo "100 regions 16 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 100) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j16 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""


### 1000 seq
echo "1000 regions 1 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j1 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""


echo "1000 regions 4 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j4 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""

echo "1000 regions 8 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j8 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""

echo "1000 regions 16 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 1000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j16 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""

### 10000 seq
echo "10000 regions 1 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j1 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""

echo "10000 regions 4 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j4 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""

echo "10000 regions 8 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j8 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""

echo "10000 regions 16 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT 10000) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j16 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""

### All the seq
echo "All the seq 1 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j1 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""

echo "All the seq 4 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j4 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""

echo "All the seq 8 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j8 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""

echo "All the seq 16 thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print "\47"$1"\47,",$2","$3}' | parallel -j16 -n 1 -q bash -c "psql jir2004 -c \"SELECT * FROM inputtest({}) \"" > /dev/null 2>&1)
echo ""
