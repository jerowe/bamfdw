#!/usr/bin/env perl 
#===============================================================================
#
#         FILE:  test.pl
#
#        USAGE:  ./test.pl  
#
#  DESCRIPTION:  
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Jillian Rowe (), jir2004@qatar-med.cornell.edu
#      COMPANY:  Weill Cornell Medical College Qatar
#      VERSION:  1.0
#      CREATED:  04/23/2015 02:45:29 PM
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;


my @jobs = (1,4,8,16);
my @seq = (100, 1000, 10000, 1);


print "echo ==========================================\n";
print "echo BamMetaCLI Jobs\n";

my $command;

foreach my $seq (@seq){
    foreach my $job (@jobs){
        if($seq != 1){
            $command =<<EOF;
echo "$seq seq $job thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT $seq) TO STDOUT WITH CSV;' | awk -F',' '{print \$1","\$2","\$3}' | parallel -n 1 -j$job -q bash -c "python bammetacli.py {}" ;  )
echo "" 
EOF
        }
        else{
            $command =<<EOF;
echo "All the seq $job thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print \$1","\$2","\$3}' | parallel -n 1 -j$job -q bash -c "python bammetacli.py {}" ;  )
echo "" 
EOF
        }
        print "$command\n";
    }
}

print "echo ==========================================\n";
print "echo BamCovCLI Jobs\n";

foreach my $seq (@seq){
    foreach my $job (@jobs){
        if($seq != 1){
            $command =<<EOF;
echo "$seq seq $job thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225 LIMIT $seq) TO STDOUT WITH CSV;' | awk -F',' '{print \$1","\$2","\$3}' | parallel -n 1 -j$job -q bash -c "python bamcovcli.py {}" ;  )
echo "" 
EOF
        }
        else{
            $command =<<EOF;
echo "All the seq $job thread"
time (psql jir2004 -c 'COPY (SELECT * FROM called_exome_targets_20110225) TO STDOUT WITH CSV;' | awk -F',' '{print \$1","\$2","\$3}' | parallel -n 1 -j$job -q bash -c "python bamcovcli.py {}" ;  )
echo "" 
EOF
        }
        print "$command\n";
    }
}
